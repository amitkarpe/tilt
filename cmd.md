kubectl create deployment  podbb --image=busybox -o yaml --dry-run=true > app.yaml
kubectl exec -it podbb-54fcd4886c-rvxhp -- curl localhost
kubectl exec -it $POD -- curl localhost
export POD=$(kubectl get pods -l "app=podbb" -o jsonpath="{.items[0].metadata.name}")
kubectl exec -it $POD -- cat /usr/share/nginx/html/index.html


kubectl run frontend --image=nginx --port=80
kubectl expose deployment frontend --port=9002 --target-port=80 --type=NodePort

curl $(minikube ip):$(kubectl get svc frontend -o json | jq  '.spec.ports[].nodePort')
